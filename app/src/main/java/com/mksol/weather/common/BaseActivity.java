package com.mksol.weather.common;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.mksol.weather.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class BaseActivity extends AppCompatActivity {

    public Context mContext;
    public Activity mActivity;

    boolean done = false;


    private AlertDialog progressDialog;
    private AlertDialog.Builder builder;

    @SuppressLint("SourceLockedOrientationActivity")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            getSupportActionBar().hide(); // hide the title bar

        } catch (Exception ee) {

        }

        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        setResult(RESULT_CANCELED);

    }

    public void showAlertDialogError(String message, String title) {
        new AlertDialog.Builder(this, R.style.AppThemeMikeProgress_Dark_Dialog)
                .setMessage(message)
                .setTitle(title)
                .setPositiveButton("Ok", null)
                .setCancelable(false)
                .show();
    }

    public void showAlertDialogFinish(String message, String title) {
        new AlertDialog.Builder(this,
                R.style.AppThemeMikeProgress_Dark_Dialog)
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .setCancelable(false)
                .show();
    }

    public void hideKeyboard(View view) {
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    protected void showProgressDialog(Context context, String message) {
        if (progressDialog != null && progressDialog.isShowing()) {
            return;
        }

        LayoutInflater li = LayoutInflater.from(this);
        View myView = li.inflate(R.layout.layout_loading_dialog, null);

        builder = new AlertDialog.Builder(context, R.style.AppThemeMikeProgress_Dark_Dialog);
        builder.setCancelable(false);

        builder.setView(myView);

        TextView txt = (TextView) myView.findViewById((R.id.textMessage));
        txt.setText(message);

        ProgressBar prog = (ProgressBar) myView.findViewById(R.id.progress);

        progressDialog = builder.create();

        progressDialog.show();
    }

    protected void hideProgressDialog() {
        if (progressDialog == null) {
            return;
        }

        progressDialog.dismiss();
    }

    public static boolean validateIDNum(String idNumber) {

        if (idNumber == null || idNumber.length() != 13)
            return false;

        char[] id = idNumber.toCharArray();
        long total_odds = 0;
        long total_even = 0;

        for (int i = 0, j = 100000; i < id.length - 1; i += 2, j /= 10) {
            total_odds += (long) id[i] - 48;
            total_even += ((long) id[i + 1] - 48) * j;
        }

        total_even *= 2;
        long total_all = total_odds;
        while (total_even != 0) {
            total_all += total_even % 10;
            total_even /= 10;
        }

        int check_number = (int) (10 - total_all % 10);
        if (check_number % 10 == (long) id[12] - 48)
            return true;
        return false;
    }

    public boolean validateEmail(String email) {
        boolean proceed = false;
        String emailPattern = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
        if (email.matches(emailPattern)) {
            proceed = true;
        }

        return proceed;
    }

    public boolean validateCellPhone(String number) {
        return android.util.Patterns.PHONE.matcher(number).matches();
    }


    public static String getError(String message) {
        String error = "Unknown error";

        message = message.replace("{\"error\":\"", "").replace("\"}", "");
        message = message.replace("id_", "ID ");


        error = message.substring(0, 1).toUpperCase() + message.substring(1).toLowerCase();

        return error;
    }

    @SuppressLint({"HardwareIds", "MissingPermission"})
    public static String getDeviceId(Context context) {

        String deviceId;

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            deviceId = Settings.Secure.getString(
                    context.getContentResolver(),
                    Settings.Secure.ANDROID_ID);
        } else {
            final TelephonyManager mTelephony = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            if (mTelephony.getDeviceId() != null) {
                deviceId = mTelephony.getDeviceId();
            } else {
                deviceId = Settings.Secure.getString(
                        context.getContentResolver(),
                        Settings.Secure.ANDROID_ID);
            }
        }

        return deviceId;
    }

    public String getSIMCCIDByCmd() {
        try {
            Process getSimCCIDProc = Runtime.getRuntime().exec(new String[]{"getprop", "ril.iccid.sim1"});
            try (final BufferedReader b = new BufferedReader(new InputStreamReader(getSimCCIDProc.getInputStream()))) {
                String ccid = b.readLine();
                return ccid;
            } catch (final IOException e) {
                return null;
            }
        }
        catch (Exception e) {

            return null;
        }
    }

    @SuppressLint("MissingPermission")
    @SuppressWarnings("deprecation")
    public String getIMEINumber() {

        String imei = "";
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (checkSelfPermission(Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
                if (telephonyManager != null) {
                    try {
                        imei = telephonyManager.getImei();
                    } catch (Exception e) {
                        e.printStackTrace();
                        imei = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
                    }
                }
            } else {
                ActivityCompat.requestPermissions(BaseActivity.this, new String[]{Manifest.permission.READ_PHONE_STATE}, 1010);
            }
        } else {
            if (ActivityCompat.checkSelfPermission(BaseActivity.this, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
                if (telephonyManager != null) {
                    imei = telephonyManager.getDeviceId();
                }
            } else {
                ActivityCompat.requestPermissions(BaseActivity.this, new String[]{Manifest.permission.READ_PHONE_STATE}, 1010);
            }
        }

        return  imei;
    }

//    public Boolean isTokenValid(String Email, String Token)
//    {
//        SoapCallHandler soap = new SoapCallHandler() {
//            @Override
//            public void onStart(boolean connected) {
//
//            }
//
//            @Override
//            public Boolean onSuccess(Object val) {
//                done = true;
//
//                Boolean res = (Boolean) val;
//
//                return res;
//
//
//            }
//
//            @Override
//            public void onFail() {
//            }
//        };
//
//        new IsTokenValid(soap, Email, Token).execute();
//        return false;
//
//    }


//    public void setupMenu(int resName, int resButton, String name) {
//        try {
//            TextView txtName = (TextView) findViewById(resName);
//            txtName.setText(name);
//        } catch (Exception e) {
//
//            e.printStackTrace();
//        }
//
//        try {
//            final ImageButton buttonMenu = (ImageButton) findViewById(resButton);
//            buttonMenu.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    PopupMenu menu = new PopupMenu(mContext, buttonMenu);
//
//                    // Call inflate directly on the menu:
//                    menu.inflate(R.menu.menu1);
//
//                    menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
//                        @Override
//                        public boolean onMenuItemClick(MenuItem item) {
//
//
//                            switch (item.getItemId()) {
//                                case R.id.itemHome:
//                                    Intent intent = new Intent(mContext, ZendeskMainActivity.class);
//                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                                    startActivity(intent);
//
//                                    finish();
//
//                                    break;
//                                case R.id.itemSignout:
//                                    SharedPreferenceManager.getInstance().save("AUTH_TOKEN", "");
//                                    SharedPreferenceManager.getInstance().save("USERNAME", "");
//                                    SharedPreferenceManager.getInstance().save("PASSWORD", "");
//                                    SharedPreferenceManager.getInstance().save("REMEMBERME", "0");
//
//                                    Intent intent2 = new Intent(mContext, LoginActivity.class);
//                                    intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                                    startActivity(intent2);
//
//                                    finish();
//
//                                    break;
//
//                            }
//
//                            return false;
//                        }
//                    });
//
//                    menu.show();
//                }
//            });
//        } catch (Exception ee) {
//
//        }
//
//
//    }
}
