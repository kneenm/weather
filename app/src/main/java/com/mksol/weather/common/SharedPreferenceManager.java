package com.mksol.weather.common;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;


import com.google.gson.Gson;
import com.mksol.weather.MikeApplication;

public final class SharedPreferenceManager {

    private static SharedPreferenceManager ourInstance = new SharedPreferenceManager();
//    private static Activity activity;

    private SharedPreferenceManager() {
    }

    public static SharedPreferenceManager getInstance() {
        return ourInstance;
    }

    public void setActivity(Activity activity) {
        MikeApplication.activity = activity;
    }

    public void clear()
    {
//        SharedPreferences sharedPref = RainApplication.activity.getPreferences(Context.MODE_PRIVATE);
//        SharedPreferences.Editor editor = sharedPref.edit();
//
//        editor.clear();
//        editor.apply();
    }

    /**
     * Save a key-value pair to SharedPreferences for Persistent Storage
     *
     * @param key
     * @param value
     */
    public void save(String key, String value) {
        SharedPreferences sharedPref =   MikeApplication.activity.getSharedPreferences("MIKEALERT", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key, value);
        editor.apply();
    }

    /**
     * Save a key-value pair where the value is an object to SharedPreferences for Persistent Storage
     *
     * @param key
     * @param value
     */
    public void saveObject(String key, Object value) {
        SharedPreferences sharedPref =   MikeApplication.activity.getSharedPreferences("MIKEALERT", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();

        Gson gson = new Gson();
        String json = gson.toJson(value);

        editor.putString(key, json);
        editor.apply();
    }

    /**
     * Retrieve a value from SharedPreferences by a key
     *
     * @param key
     * @return
     */
    public String get(String key) {
        return get(key, "");
    }

    /**
     * Retrieve a value from SharedPreferences by a key, use default value if it does not exist
     *
     * @param key
     * @param default_value
     * @return
     */
    public String get(String key, String default_value) {
        try
        {
            SharedPreferences sharedPref =  MikeApplication.activity.getSharedPreferences("MIKEALERT", Context.MODE_PRIVATE);
            return sharedPref.getString(key, default_value);

        }
        catch (Exception e)
        {
            e.printStackTrace();
            return "";
        }
    }

}

