package com.mksol.weather.classes;

import java.util.ArrayList;

public class ForecastList {
    public int dt;
    public Main main;
    public ArrayList<Weather> weather;
    public Clouds clouds;
    public Wind wind;
    public int visibility;
    public double pop;
    public Sys sys;
    public String dt_txt;
//    public Rain rain;
}
