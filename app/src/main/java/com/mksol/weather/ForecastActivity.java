package com.mksol.weather;

import static com.mksol.weather.MikeApplication.mUnits;
import static com.mksol.weather.common.Globals.API;

import android.os.AsyncTask;
import android.os.Bundle;

import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.TextView;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.gson.Gson;
import com.mksol.weather.classes.Forecast;
import com.mksol.weather.classes.ForecastList;
import com.mksol.weather.classes.MinMaxClass;
import com.mksol.weather.common.BaseActivity;
import com.mksol.weather.common.HttpRequest;
import com.mksol.weather.databinding.ActivityForecastBinding;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class ForecastActivity extends BaseActivity {

    TextView addressTxt, updated_atTxt, statusTxt, tempTxt, temp_minTxt, temp_maxTxt, sunriseTxt,
            sunsetTxt, windTxt, pressureTxt, humidityTxt;

    String mUnits;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forecast);

        addressTxt = findViewById(R.id.address);
        updated_atTxt = findViewById(R.id.updated_at);
        statusTxt = findViewById(R.id.status);
        tempTxt = findViewById(R.id.temp);
        temp_minTxt = findViewById(R.id.temp_min);
        temp_maxTxt = findViewById(R.id.temp_max);
        sunriseTxt = findViewById(R.id.sunrise);
        sunsetTxt = findViewById(R.id.sunset);
        windTxt = findViewById(R.id.wind);
        pressureTxt = findViewById(R.id.pressure);
        humidityTxt = findViewById(R.id.humidity);


        Long updatedAt = (long) ((MikeApplication) getApplication()).mWeather.getDt();
        String updatedAtText = "Updated at: " + new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.ENGLISH).format(new Date(updatedAt * 1000));

        mUnits = ((MikeApplication) getApplication()).mUnits;

        String tempMin = "Min Temp: " + ((MikeApplication) getApplication()).mWeather.getMain().getTemp_min() + "° C";
        String tempMax = "Max Temp: " + ((MikeApplication) getApplication()).mWeather.getMain().getTemp_max() + "° C";
        String temp = ((MikeApplication) getApplication()).mWeather.getMain().getTemp() + "° C";

        if (mUnits.toLowerCase(Locale.ROOT).startsWith("i"))
        {
            tempMin = "Min Temp: " + ((MikeApplication) getApplication()).mWeather.getMain().getTemp_min() + "° F";
            tempMax = "Max Temp: " + ((MikeApplication) getApplication()).mWeather.getMain().getTemp_max() + "° F";
            temp = ((MikeApplication) getApplication()).mWeather.getMain().getTemp() + "° F";
        }

        String pressure = "" + ((MikeApplication) getApplication()).mWeather.getMain().getPressure();
        String humidity = "" + ((MikeApplication) getApplication()).mWeather.getMain().getHumidity();

        Long sunrise = (long) ((MikeApplication) getApplication()).mWeather.getSys().getSunrise();
        Long sunset = (long) ((MikeApplication) getApplication()).mWeather.getSys().getSunset();
        String windSpeed = "" + ((MikeApplication) getApplication()).mWeather.getWind().getSpeed();

        String address = ((MikeApplication) getApplication()).mWeather.getName() +", " + ((MikeApplication) getApplication()).mWeather.getSys().getCountry();


        addressTxt.setText(address);
        updated_atTxt.setText(updatedAtText);
        tempTxt.setText(temp);
        temp_minTxt.setText(tempMin);
        temp_maxTxt.setText(tempMax);
        sunriseTxt.setText(new SimpleDateFormat("hh:mm a", Locale.ENGLISH).format(new Date(sunrise * 1000)));
        sunsetTxt.setText(new SimpleDateFormat("hh:mm a", Locale.ENGLISH).format(new Date(sunset * 1000)));
        windTxt.setText(windSpeed);
        pressureTxt.setText(pressure);
        humidityTxt.setText(humidity);

        new weatherTask().execute();
    }

    class weatherTask extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            /* Showing the ProgressBar, Making the main design GONE */
            findViewById(R.id.loader).setVisibility(View.VISIBLE);
            findViewById(R.id.mainContainer).setVisibility(View.GONE);
            findViewById(R.id.errorText).setVisibility(View.GONE);
        }

        protected String doInBackground(String... args) {
            // imperial
            String response = HttpRequest.excuteGet("https://api.openweathermap.org/data/2.5/forecast?lat=" + ((MikeApplication) getApplication()).mLat +
                    "&lon=" + ((MikeApplication) getApplication()).mLong + "&units=" + ((MikeApplication) getApplication()).mUnits + "&appid=" + API);
            return response;
        }

        @Override
        protected void onPostExecute(String result) {

            List<MinMaxClass> minmax = new ArrayList();

            try {
                Gson gson = new Gson();
                Forecast signInData = gson.fromJson(result, Forecast.class);

                for (int i = 0 ; i < signInData.list.size(); i++)
                {
                    String dt = signInData.list.get(i).dt_txt.substring(8, 10);
                    String tp = "" + signInData.list.get(i).main.getTemp();
                    boolean exists = false;

                        for (int j = 0; j < minmax.size(); j++)
                        {
                            if (minmax.get(j).Day == Integer.parseInt(dt))
                            {
                                double min = minmax.get(j).Min;
                                double max = minmax.get(j).Max;
                                double mn = Math.min(min, Double.parseDouble(tp));
                                double ma = Math.max(max, Double.parseDouble(tp));
                                minmax.get(j).Min = (int)mn;
                                minmax.get(j).Max = (int)ma;

                                exists = true;
                            }
                        }


                    if (!exists)
                    {
                        MinMaxClass mm = new MinMaxClass();
                        mm.Day = Integer.parseInt(dt);
                        mm.Min = (int)Double.parseDouble(tp);
                        mm.Max = (int)Double.parseDouble(tp);

                        minmax.add(mm);

                    }
                }

                for (int i = 0 ; i < minmax.size(); i++)
                {
                    String dt = "" + minmax.get(i).Day;
                    String tp = "" + minmax.get(i).Min + "/" + minmax.get(i).Max;

                    tp += "°";

                    if (mUnits.startsWith("m"))
                        tp = tp + " C";
                    else
                        tp = tp + " F";

                    if (i == 0)
                    {
                        TextView dte = (TextView)findViewById(R.id.day1Day);
                        TextView tmp = (TextView)findViewById(R.id.day1Temp);

                        dte.setText(dt);
                        tmp.setText(tp);
                    }
                    else if (i == 1)
                    {
                        TextView dte = (TextView)findViewById(R.id.day2Day);
                        TextView tmp = (TextView)findViewById(R.id.day2Temp);

                        dte.setText(dt);
                        tmp.setText(tp);
                    }
                    else if (i == 2)
                    {
                        TextView dte = (TextView)findViewById(R.id.day3Day);
                        TextView tmp = (TextView)findViewById(R.id.day3Temp);

                        dte.setText(dt);
                        tmp.setText(tp);
                    }
                    else if (i == 3)
                    {
                        TextView dte = (TextView)findViewById(R.id.day4Day);
                        TextView tmp = (TextView)findViewById(R.id.day4Temp);

                        dte.setText(dt);
                        tmp.setText(tp);
                    }
                    else if (i == 4)
                    {
                        TextView dte = (TextView)findViewById(R.id.day5Day);
                        TextView tmp = (TextView)findViewById(R.id.day5Temp);

                        dte.setText(dt);
                        tmp.setText(tp);
                    }

//                    else if (i > 4)
//                        break;
                }

                findViewById(R.id.loader).setVisibility(View.GONE);
                findViewById(R.id.mainContainer).setVisibility(View.VISIBLE);
                findViewById(R.id.errorText).setVisibility(View.GONE);

                Object obj = signInData;

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}