package com.mksol.weather;

import android.app.Activity;
import android.app.Application;

import com.mksol.weather.classes.Weather;

public class MikeApplication extends Application {
    public static Activity activity;

    public static Weather mWeather;
    private static MikeApplication context;

    public static String mUnits;
    public static Double mLat;
    public static Double mLong;

    @Override
    public void onCreate() {
        super.onCreate();

        context = this;
    }

    public static MikeApplication getInstance() {
        return context;
    }

}
