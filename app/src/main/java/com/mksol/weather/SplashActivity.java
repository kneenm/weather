package com.mksol.weather;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.mksol.weather.common.SharedPreferenceManager;


public class SplashActivity extends AppCompatActivity {
    Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;

        SharedPreferenceManager.getInstance().setActivity(this);

    }

    @Override
    protected void onResume() {
        super.onResume();

        try {
            Thread.sleep(2000);
        }
        catch (Exception e) {

        }
        new Thread(new Task()).start();
    }

    class Task implements Runnable {
        @Override
        public void run() {
            startActivity(new Intent(mContext, MapsActivity.class));
            finish();
        }
    }

}

