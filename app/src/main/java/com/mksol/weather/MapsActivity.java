package com.mksol.weather;

import static com.mksol.weather.common.Globals.API;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SearchView;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.mksol.weather.classes.Main;
import com.mksol.weather.classes.Sys;
import com.mksol.weather.classes.Weather;
import com.mksol.weather.classes.Wind;
import com.mksol.weather.common.BaseActivity;
import com.mksol.weather.common.HttpRequest;
import com.mksol.weather.databinding.ActivityMapsBinding;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class MapsActivity extends BaseActivity implements OnMapReadyCallback, LocationListener, GoogleMap.OnInfoWindowClickListener {

    private GoogleMap mMap;
    private ActivityMapsBinding binding;

    public double mLat, mLong;
    public LocationManager locationManager;

    private String mUnits = "metric";

    SearchView searchView;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = this;
        mActivity = this;

        try {

        if (needsPermission()) {
            String[] perms = retrievePermissions(mContext);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(perms, 10);
            }
        } else {

            continueStart();
        }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public static String[] retrievePermissions(Context context) {
        try {
            return context
                    .getPackageManager()
                    .getPackageInfo(context.getPackageName(), PackageManager.GET_PERMISSIONS)
                    .requestedPermissions;
        } catch (PackageManager.NameNotFoundException e) {
            throw new RuntimeException("This should have never happened.", e);
        }
    }

    public boolean hasAllPermissionsGranted(int[] grantResults) {
        for (int grantResult : grantResults) {
            if (grantResult == PackageManager.PERMISSION_DENIED) {
                return false;
            }
        }
        return true;
    }

    public boolean needsPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            return true;
        else
            return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 10) {
            if (hasAllPermissionsGranted(grantResults)) {
                continueStart();
            } else {
//                showAlertDialog("Required permission has not been granted", "Error");
                continueStart();
//                finish();
            }
        }
    }

    public void continueStart() {

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
//            return;
        }
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
        Location loc = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        mLat = loc.getLatitude();
        mLong = loc.getLongitude();

        setContentView(R.layout.activity_maps);

        Button btnC = (Button) findViewById(R.id.buttonC);
        Button btnF = (Button) findViewById(R.id.buttonF);

        btnC.setTextColor(Color.parseColor("#000000"));
        btnF.setTextColor(Color.parseColor("#ffffff"));

        btnC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mUnits = "metric";

                new weatherTask().execute();

                btnC.setTextColor(Color.parseColor("#000000"));
                btnF.setTextColor(Color.parseColor("#ffffff"));

            }
        });

        btnF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mUnits = "imperial";

                new weatherTask().execute();

                btnF.setTextColor(Color.parseColor("#000000"));
                btnC.setTextColor(Color.parseColor("#ffffff"));

            }
        });


        searchView = findViewById(R.id.idSearchView);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                String location = searchView.getQuery().toString();

                List<Address> addressList = null;

                if (location != null || location.equals("")) {
                    Geocoder geocoder = new Geocoder(MapsActivity.this);
                    try {
                        addressList = geocoder.getFromLocationName(location, 1);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    Address address = addressList.get(0);

                    LatLng latLng = new LatLng(address.getLatitude(), address.getLongitude());
                    mLat = latLng.latitude;
                    mLong = latLng.longitude;

                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10));

                    searchView.setQuery("", false);
                    searchView.clearFocus();

                    new weatherTask().execute();

                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setOnInfoWindowClickListener(this);
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(@NonNull LatLng latLng) {
                mMap.clear();

                mLat = latLng.latitude;
                mLong = latLng.longitude;

                new weatherTask().execute();
            }
        });

        new weatherTask().execute();

    }

    public void doWork(String temp) {
        mMap.clear();

        LatLng sydney = new LatLng(mLat, mLong);
        Marker locationMarker = mMap.addMarker(new MarkerOptions().position(sydney).title(temp).visible(true));

        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(sydney, 18));
        locationMarker.showInfoWindow();
    }

    @Override
    public void onInfoWindowClick(@NonNull Marker marker) {

        Intent inte = new Intent(mContext, ForecastActivity.class);
        startActivity(inte);

    }

    public class weatherTask extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        protected String doInBackground(String... args) {
            String response = HttpRequest.excuteGet("https://api.openweathermap.org/data/2.5/weather?lat=" + mLat +
                    "&lon=" + mLong + "&units=" + mUnits + "&appid=" + API);
            return response;
        }

        @Override
        protected void onPostExecute(String result) {


            try {
                Gson gson = new Gson();
                Weather signInData = gson.fromJson(result, Weather.class);
                JSONObject jsonObj = new JSONObject(result);

                JSONObject main = jsonObj.getJSONObject("main");
                JSONObject sys = jsonObj.getJSONObject("sys");
                JSONObject wind = jsonObj.getJSONObject("wind");
//                JSONObject weather = jsonObj.getJSONArray("weather").getJSONObject(0);


                Sys syst = gson.fromJson(sys.toString(), Sys.class);
                signInData.setSys(syst);

                Main maint = gson.fromJson(main.toString(), Main.class);
                signInData.setMain(maint);

                Wind wint = gson.fromJson(wind.toString(), Wind.class);
                signInData.setWind(wint);

                ((MikeApplication) getApplication()).mWeather = signInData;
                ((MikeApplication) getApplication()).mUnits  = mUnits;
                ((MikeApplication) getApplication()).mLat  = mLat;
                ((MikeApplication) getApplication()).mLong  = mLong;

                String tem = signInData.getMain().getTemp() + "°";

                if (mUnits.startsWith("m"))
                    tem = tem + " C";
                else
                    tem = tem + " F";

                doWork(tem);

            } catch (JSONException e) {
            }

        }
    }

    @Override
    public void onLocationChanged(@NonNull Location location) {

    }
}